Prerequisite to run this app (It could be imported as eclipse project and run from there):

*  install Java 8 JDK
*  install MAVEN (not required if you run from eclipse: use command *Run As - Maven Build - type command*)


**Components**:

* JAX-RS 2 & Jersey 2.22.2 RESTful service 
* Jersey Async API & Java 8 Async (CompletableFuture) 
* Force.com Rest API & Wave Rest API
* Java 8 stream API 
* AWS S3 Multipart Upload API

Notes: all required jars are declared pom.xml dependancy element and are copied during project jar/war build.

**Service definition:**

POST *platform_endpoint:port/wave/async*
Headers: Content-Type application/json 
Body: 


```
#!json

{
"query" : "q = load \"0Fb1a000000CcqqCAC/0Fc1a0000004cAcCAI\";q = filter q by 'Date_Day' == \"{1}\" ;q = foreach q generate 'Origin' as 'Origin', 'Destination' as 'Destination', 'DestinationCity' as 'DestinationCity', 'OriginCity' as 'OriginCity', 'Cancelled' as 'Cancelled', 'Diverted' as 'Diverted', 'Date' as 'Date', 'OriginState' as 'OriginState', 'DestinationState' as 'DestinationState', 'Carrier' as 'Carrier', 'DepartureTime' as 'DepartureTime', 'ArrivalDelay' as 'ArrivalDelay', 'DepDelayMinutes' as 'DepDelayMinutes', 'Flights' as 'Flights', 'WheelsOff' as 'WheelsOff', 'AirTime' as 'AirTime', 'DistanceMiles' as 'DistanceMiles', 'ElapsedTime' as 'ElapsedTime', 'WheelsOn' as 'WheelsOn', 'ArrivalTime' as 'ArrivalTime';q=order q by 'Carrier';q = limit q {0};", 
"groupBy":"Date_Day",
"dataset":"0Fb1a000000CcqqCAC/0Fc1a0000004cAcCAI"
}
```


To run on **heroku**:

* check pom.xml and set packaging as *jar*
* run command

```
#!command line

mvn clean heroku:deploy
```
*Notes:* 
* *app is running within HTTP Server Grizzly 2*

* *the idea is to split the whole dataset into more or less equally distributed chunks grouping by Date_Day and to iterate over the whole dataset using filter clause with group and limit with count of records within that group *

* *offset is very expensive operation*

* *order is important; use filter clause right after load clause*
	

TBD!!!	
*To run on **aws beanstalk**: 

* check pom.xml and set packaging as *war*
* run command

```
#!command line

mvn clean install
```

* deploy *project_root/target/*.war* into aws beanstalk environment with web server Tomcat

*Notes: app is running within Web Server Tomcat based on web.xml declaration**