package com.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;



public class WaveQueryService {
	
	private static WaveQueryService instance = null;
	 
	protected WaveQueryService() {
	}
 
	public static WaveQueryService getInstance() {
		if (instance == null) {
			// Thread Safe. Might be costly operation in some case
			synchronized (WaveQueryService.class) {
				if (instance == null) {
					instance = new WaveQueryService();
				}
			}
		}
		return instance;
	}

	private static final String ENDPOINT_V36_WAVE_QUERY = "/services/data/v36.0/wave/query";
	//TODO move to config or oauth
	String endpoint = "https://login.salesforce.com/services/oauth2/token";
	String username = "natallia.bahlai@analytics.cloudsherpas.com";
	String password = "salesforce_2016gyBozJh2NDg3m7PjeqcIjEgJo";
	String clientId = "3MVG9sG9Z3Q1RlbeVwQChjVZXr38B0EDcUwbyYBcHsYbifD3QqU_HQEbBaTRDAzCDWTtdJEguFsp_ON63Cohy";
	String clientSecret = "6612804141432217070";

	String accessToken;
	String instanceUrl;

	public boolean login() {

		HttpClient httpclient = new HttpClient();

		// TODO oauth
		PostMethod post = new PostMethod(endpoint);
		post.addParameter("username", username);
		post.addParameter("password", password);
		post.addParameter("grant_type", "password");
		post.addParameter("client_id", clientId);
		post.addParameter("client_secret", clientSecret);

		try {
			httpclient.executeMethod(post);
			JSONObject authResponse = new JSONObject(new JSONTokener(
					new InputStreamReader(post.getResponseBodyAsStream())));

			accessToken = authResponse.getString("access_token");
			instanceUrl = authResponse.getString("instance_url");

		} catch (IOException ex) {
			return false;
		} finally {
			post.releaseConnection();
		}
		return true;
	}
	
	public Map<String, Integer> runGroupQuery(String dataset, String groupBy) {
		String query = "q = load \"{0}\";q = group q by \''{1}'\';q = foreach q generate \''{1}\'' as 'Group', count() as 'Count';q = limit q 100;"
		;
		
		PostMethod post = new PostMethod(instanceUrl
				+ ENDPOINT_V36_WAVE_QUERY);
		post.addRequestHeader("Authorization", "Bearer " + accessToken);
		
		String body = new JSONObject().put("query", MessageFormat.format(query, dataset, groupBy))
				.toString();

		try {
			post.setRequestEntity(new StringRequestEntity(body,
					MediaType.APPLICATION_JSON, "UTF-8"));
			new HttpClient().executeMethod(post);
			if (post.getStatusCode() != HttpStatus.SC_OK) {
				System.out.println(post.getResponseBodyAsString(100));
				return null;
			}

			JSONArray output = new JSONObject(new JSONTokener(
					new InputStreamReader(post.getResponseBodyAsStream()))).getJSONObject("results").getJSONArray("records");			
			
			Map<String, Integer> result = new HashMap<String, Integer>();
			for (Object e : output) {//TODO: check StreamSupport.stream(output.spliterator(), false).flatMap(mapper)
				JSONObject json = (JSONObject) e;
				result.put(json.getString("Group"), json.getInt("Count"));
			}

			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	

	//TODO move to AWSS3Service
	public class FilePart {
		public InputStream stream;
		//public String content;
		public Integer index;
		public FilePart(InputStream stream, Integer index) {
			this.stream = stream;
			this.index = index;
		}
/*
		public boolean isLess5Mb() {
			try {
				return stream.available() < AWSS3Service.DEFAULT_FILE_PART_SIZE;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}*/
	}
	
	public FilePart runWaveQuery(String query, Integer i, String ... args) {
		try {
			System.out.println(Thread.currentThread().getName() +"| group="+ args[1]+", limit="+args[0]);
			HttpClient httpclient = new HttpClient();

			PostMethod post = new PostMethod(instanceUrl
					+ ENDPOINT_V36_WAVE_QUERY);
			post.addRequestHeader("Authorization", "Bearer " + accessToken);

			String body = new JSONObject().put("query", MessageFormat.format(query, args))
					.toString();

			post.setRequestEntity(new StringRequestEntity(body,
					MediaType.APPLICATION_JSON, "UTF-8"));

			httpclient.executeMethod(post);
			System.out.println(Thread.currentThread().getName() + "| SAQL status: " + post.getStatusCode());
			
			if (post.getStatusCode() == HttpStatus.SC_OK){
				return new FilePart(post.getResponseBodyAsStream(), i);
			}
			else {
				//System.out.println(Thread.currentThread().getName() + " | " + IOUtils.toString(post.getResponseBodyAsStream()));
				return runWaveQuery(query, i, args);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
