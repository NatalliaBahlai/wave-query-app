package com.resources;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AbortMultipartUploadRequest;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.UploadPartRequest;

public class AWSS3Service {
	public static final long DEFAULT_FILE_PART_SIZE = 5 * 1024 * 1024; // 5MB
	
	final String bucketName = "wavecsv";
	final String filename = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss").format(new Date()) + ".csv";// Decouple
	final String uploadId;
	final List<PartETag> partETags;
	final AmazonS3 s3Client;
	//Integer partIndex = 1;
	
	public AWSS3Service (Integer capacity) {
		// create a client connection based on credentials
		s3Client = new AmazonS3Client(new BasicAWSCredentials(
				"AKIAIFAES54S5ZTIEFOA", "aJSB2kSUh3XuHw87pcQX4aYxmuTJeue2xpj54IMw"));//TODO move to config
		// Create a list of UploadPartResponse objects. You get one of these for each part upload.
		// Step 1: Initialize.
		InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(
				bucketName, filename);
		InitiateMultipartUploadResult initResponse = s3Client
				.initiateMultipartUpload(initRequest);
		uploadId = initResponse.getUploadId();
		partETags = new ArrayList<PartETag>(capacity);
	}
	/*
	public synchronized Integer getPartIndex() {
		return partIndex;
	}
	
	public synchronized void setPartIndex(Integer partIndex) {
		this.partIndex = partIndex;
	}
	*/
	
	public Integer append(String content, Integer i)  {
		//Integer i = getPartIndex();
		System.out.println(Thread.currentThread().getName() + "| part #: " + i + "| key: " + filename);
		if (content == null) return null;
		
		ByteArrayInputStream bi = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));

		long partSize = bi.available(); // Set part size to 5 MB.

		if (partSize < DEFAULT_FILE_PART_SIZE) return null;
		try {
			// Step 2: Upload parts.
			// Last part can be less than 5 MB. Adjust part size.
			// partSize = Math.min(partSize, (contentLength - filePosition));

			// Create request to upload a part.
			UploadPartRequest uploadRequest = new UploadPartRequest()
					.withBucketName(bucketName)
					.withKey(filename)
					.withUploadId(uploadId)
					.withPartNumber(i)
					//.withFileOffset(filePosition)
					.withInputStream(bi)
					.withPartSize(partSize);

			// Upload part and add response to our list.
			PartETag tag = s3Client
					.uploadPart(uploadRequest)
					.getPartETag();
			partETags.add( tag);

			System.out.println("Part Upload has been done successfully.");
			return i;
		} catch (Exception e) {
			e.printStackTrace();
			s3Client.abortMultipartUpload(new AbortMultipartUploadRequest(
					bucketName, filename, uploadId));
		}
		finally {
			//setPartIndex(i + 1);
		}
		
		return null;
	}

	public String finish(List<PartETag> partETags) {
		try {
			// Step 3: Complete.
			System.out.println(this.partETags.size());
			CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(
					bucketName, filename, uploadId, this.partETags);
			s3Client.completeMultipartUpload(compRequest);
			s3Client.setObjectAcl(bucketName, filename, CannedAccessControlList.PublicRead);
				
			return publishUrl();
		} catch (Exception e) {
			e.printStackTrace();
			s3Client.abortMultipartUpload(new AbortMultipartUploadRequest(
					bucketName, filename, uploadId));
		}
		return null;
	}

	private String publishUrl() {
		Date date = Date.from(LocalDate.now().plusYears(1)
				.atStartOfDay(ZoneId.systemDefault()).toInstant());
		String url = s3Client.generatePresignedUrl(bucketName, filename,
				date).toString();
		System.out.println(url);
		return url;
	}

}