package com.resources;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.glassfish.jersey.server.ManagedAsync;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

@Path("/wave")
@Produces(MediaType.TEXT_PLAIN)
public class WaveQueryResource {

	@POST
	@Path("/async")
	@Consumes(MediaType.APPLICATION_JSON)
	@ManagedAsync
	public void onDownload2(@Suspended final AsyncResponse ar,
			final String queriesJson) throws URIException {
		String filename = URIUtil
				.encodePath("https://s3.amazonaws.com/wavecsv/");

		ar.setTimeout(1, TimeUnit.SECONDS);
		ar.setTimeoutHandler(asyncResponse -> asyncResponse
				.resume(Response
						.status(Response.Status.ACCEPTED)
						.entity("Your request has been accepted. In N mins you can check result csv here: <url>").build()));
		//CompletableFuture.runAsync(() -> execute(queriesJson));
		execute2(queriesJson);
	}

	private void execute2(String queriesJson) {
		System.out.println(Thread.currentThread().getName()+" | MAIN");
		
		JSONObject req = new JSONObject(queriesJson);
		final String dataset = req.getString("dataset");
		final String query = req.getString("query");
		final String groupBy = req.getString("groupBy");
		
		if (query == null || groupBy == null || dataset == null) return;
		
		if (WaveQueryService.getInstance().login()) {
			
			Map<String, Integer> groups = WaveQueryService.getInstance().runGroupQuery(dataset, groupBy);
			
			AWSS3Service uploadToS3 = new AWSS3Service(groups.size());
			final ExecutorService executorService = Executors.newFixedThreadPool(groups.size());
			
			List<CompletableFuture<Integer>> all = groups.keySet()
				.stream()
				.map(group -> WaveQueryService.getInstance().runWaveQuery(query, Integer.valueOf(group), String.valueOf(groups.get(group)), group))
				.map(filePart -> CompletableFuture.supplyAsync(() ->uploadToS3.append(convertToCSV(filePart.stream), filePart.index), executorService ))
				.collect(Collectors.<CompletableFuture<Integer>>toList());
				
			finish(all, uploadToS3);
		}
	}
	
	public void finish(List<CompletableFuture<Integer>> allAsyncReqs, AWSS3Service uploadToS3) {
		CompletableFuture<Void> allDoneFuture =
		        CompletableFuture.allOf(allAsyncReqs.toArray(new CompletableFuture[allAsyncReqs.size()]));
	    allDoneFuture
	    .thenApply(v ->
	        allAsyncReqs.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.<Integer>toList())
	    )
	    .whenComplete((list, throwable) -> {
	    	uploadToS3.finish(null);
        });
	}
	
	public String convertToCSV(InputStream data)  {
		try {
			JSONArray output = new JSONObject(new JSONTokener(
					new InputStreamReader(data)))
				.getJSONObject("results")
				.getJSONArray("records");
			//System.out.println(output != null && output.length() > 0);
			if (output != null && output.length() > 0) {
				return CDL.toString(output);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}